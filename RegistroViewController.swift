//
//  RegistroViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 13/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class RegistroViewController: UIViewController {

    @IBOutlet var nomeField: UITextField!
    
    
    @IBOutlet var emailField: UITextField!
    
    @IBOutlet var senhaField: UITextField!
    
    var alerta = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.whiteColor()
       
        
        self.title = "Registre-se"
        
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cadastrar(sender: AnyObject) {
        
        if (!isValidEmail(self.emailField.text)) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Digite um e-mail válido."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if (count(self.senhaField.text) < 6) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Sua senha deve conter no mínimo 6 caracteres."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        var inNome = nomeField.text
        
        if(inNome.isEmpty){
            let alert2 = UIAlertView()
            alert2.title = "Erro!"
            alert2.message = "Nome Invalido"
            alert2.addButtonWithTitle("OK")
            alert2.show()
            return
        }
        
        self.alerta.message = "Cadastrando..."
        
        self.alerta.show()
        
        self.makeLogin()
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func makeLogin(){
        var manager = AFHTTPRequestOperationManager()
        
        var params = ["email": self.emailField.text, "password": self.senhaField.text, "username": self.nomeField.text]
        manager.POST("http://45.55.74.184:8080/users/sign_in/", parameters: params, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            
            self.alerta.dismissWithClickedButtonIndex(0, animated: true)
            
            var token = responseObject.objectForKey("token") as! String
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(token, forKey: "token")
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
            var alerta3 = UIAlertView()
            alerta3.title = "Sucesso"
            alerta3.message = "Cadastrado com sucesso"
            alerta3.addButtonWithTitle("OK")
            alerta3.show()
            
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                var errorMessage: String? = operation.responseString
                var errorAlert = UIAlertView()
                errorAlert.title = "Erro!"
                errorAlert.message = errorMessage
                errorAlert.addButtonWithTitle("OK")
                errorAlert.show()
                
                self.alerta.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
