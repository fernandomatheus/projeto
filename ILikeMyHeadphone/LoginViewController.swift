//
//  LoginViewController.swift
//  ILikeMyHeadphone
//
//  Created by Edgard Matos on 09/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var loading = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -100.0
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
        if (!isValidEmail(self.emailField.text)) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Digite um e-mail válido."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        if (count(self.passwordField.text) < 6) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Sua senha deve conter no mínimo 6 caracteres."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        
        self.loading.message = "Realizando login..."
        self.loading.show()
        
//        let alert = UIAlertView()
//        alert.title = "OK"
//        alert.message = "Formulário Válido!"
//        alert.addButtonWithTitle("OK")
//        alert.show()
        
        self.makeLogin()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func makeLogin(){
        var manager = AFHTTPRequestOperationManager()
        
        var params = ["email": self.emailField.text, "password": self.passwordField.text]
        manager.POST("http://45.55.74.184:8080/users/sign_in/", parameters: params, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
        
            self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            var token = responseObject.objectForKey("token") as! String
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(token, forKey: "token")
            
            self.navigationController?.popToRootViewControllerAnimated(true)
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Login ou senha invalida!"
                alert.addButtonWithTitle("OK")
                alert.show()
                
                self.passwordField.text = ""
                
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
        }
                
        
    }

    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
