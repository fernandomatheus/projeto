//
//  FeedViewController.swift
//  ILikeMyHeadphone
//
//  Created by Edgard Matos on 09/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var loading = UIAlertView()
    var reviews: [Review] = []

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        var login = false
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.objectForKey("token") as? String {
            login = true
        }
        
        if !login {
            self.performSegueWithIdentifier("login", sender: self)
        }
        
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.title = "I Like My Headphone"
        
        //self.getReviews()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.objectForKey("token") as? String {
            self.loading = UIAlertView()
            self.loading.message = "Buscando Reviews..."
            self.loading.show()
            
            self.getReviews()
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return reviews.count
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        var cell: FeedTableViewCell = tableView.dequeueReusableCellWithIdentifier("feedCell", forIndexPath: indexPath) as! FeedTableViewCell
        
        var review: Review = self.reviews[indexPath.row]
        cell.brandLabel.text = review.brand
        cell.commentLabel.text = review.comment
        cell.modelLabel.text = review.model
        
        cell.photoImageView.setImageWithUrl(NSURL(string: review.photo!)!, placeHolderImage: UIImage(named: "btn-photo-upload"))
        cell.photoImageView.layer.cornerRadius = 34.5
        
        var ratingName = "rating-\(review.rating!)-stars"
        cell.ratingImageView.image = UIImage(named: ratingName)
        
        return cell
        
        
    }
    
    func getReviews(){
        
        var manager = AFHTTPRequestOperationManager()
        manager.GET("http://45.55.74.184:8080/publications/list/", parameters: nil, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!)-> Void in
            
            var objects:[NSDictionary] = responseObject.objectForKey("publications") as! [NSDictionary]
            
            for object: NSDictionary in objects{
                var review = Review()
                
                review.brand = object["brand"] as? String
                review.model = object["model"] as? String
                review.rating = object["rating"] as? Int
                review.comment = object["comment"] as? String
                review.photo = object["photo"] as? String
                
                self.reviews.append(review)
                
                
            }
            
            self.tableView.reloadData()
            self.loading.dismissWithClickedButtonIndex(0, animated: true)
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                let alert = UIAlertView()
                alert.title = "Erro!"
                alert.message = "Erro interno do servidor"
                alert.addButtonWithTitle("OK")
                alert.show()
                
                self.loading.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
        
        
        }
        
        
        
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("reviewDetail", sender: self)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "reviewDetail" {
////            var viewController:FeedDetailViewController = segue.destinationViewController as! FeedDetailViewController
//            
//            var indexPath: NSIndexPath! = self.tableView.indexPathForSelectedRow()
//            var review:Review = self.reviews[indexPath.row]
////            viewController.review = review
////            viewController.onlyShow = true
//            
//            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        } else if segue.identifier == "reviewAdd" {
//            //
//        }
//        
//    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
