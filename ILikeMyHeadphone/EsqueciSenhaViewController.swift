//
//  EsqueciSenhaViewController.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 13/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class EsqueciSenhaViewController: UIViewController {

    
    @IBOutlet var emailField: UITextField!
    
    var alert = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        
        
        self.navigationController!.navigationBar.barTintColor = UIColor(red: 61.0/255.0, green: 88.0/255.0, blue: 148.0/255.0, alpha: 1.0)
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        self.title = "Esqueci Minha Senha"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    
    @IBAction func recuperar(sender: AnyObject) {
        
        if (!isValidEmail(self.emailField.text)) {
            let alert = UIAlertView()
            alert.title = "Erro!"
            alert.message = "Digite um e-mail válido."
            alert.addButtonWithTitle("OK")
            alert.show()
            return
        }
        
        self.alert.message = "Enviando nova senha..."
        alert.show()
        
        self.makeLogin()
        
        
        
    }
    
    func makeLogin(){
        var manager = AFHTTPRequestOperationManager()
        
        var params = ["email": self.emailField.text]
        manager.POST("http://45.55.74.184:8080/sign/request_reset_password/", parameters: params, success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) -> Void in
            
            self.alert.dismissWithClickedButtonIndex(0, animated: true)
            
            var token = responseObject.objectForKey("token") as! String
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(token, forKey: "token")
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
            var alerta3 = UIAlertView()
            alerta3.title = "Sucesso"
            alerta3.message = "Senha enviada!"
            alerta3.addButtonWithTitle("OK")
            alerta3.show()
            
            
            }) { (operation:AFHTTPRequestOperation!, error:NSError) -> Void in
                var errorMessage: String? = operation.responseString
                var errorAlert = UIAlertView()
                errorAlert.title = "Erro!"
                errorAlert.message = errorMessage
                errorAlert.addButtonWithTitle("OK")
                errorAlert.show()
                
                self.alert.dismissWithClickedButtonIndex(0, animated: true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.removeObjectForKey("token")
        }
        
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
