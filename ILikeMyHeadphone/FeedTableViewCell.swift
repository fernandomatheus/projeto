//
//  FeedTableViewCell.swift
//  ILikeMyHeadphone
//
//  Created by aluno-r17 on 10/07/15.
//  Copyright (c) 2015 Deway. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    @IBOutlet var brandLabel: UILabel!
    
    @IBOutlet var photoImageView: UIImageView!
    
    @IBOutlet var commentLabel: UILabel!
    
    @IBOutlet var modelLabel: UILabel!
    
    @IBOutlet var ratingImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
